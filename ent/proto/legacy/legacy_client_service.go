package legacy

// This file is NOT auto-generated

import (
	context "context"
	"encoding/base64"
	"fmt"
	"log"
	reflect "reflect"
	"strconv"
	"strings"
	"time"

	gosql "database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/patrickmn/go-cache"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

// ClientService implements ClientServiceServer
type ClientService struct {
	client       *sqlx.DB
	cache        *cache.Cache
	cacheEnabled bool
	dbName       string
	UnimplementedClientServiceServer
}

// NewClientService returns a new ClientService
func NewClientService(client *sqlx.DB, dbName string) *ClientService {
	return &ClientService{
		client:       client,
		dbName:       dbName,
		cache:        cache.New(5*time.Minute, 10*time.Minute),
		cacheEnabled: true,
	}
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func parseRows(rows *sqlx.Rows) (*DataTable, error) {
	data := &DataTable{}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	// check for duplicate column names
	for i, s := range cols {
		d := 0
		for contains(cols[0:i], s) {
			// name is already used, add suffic
			s = fmt.Sprintf("%s%d", cols[i], d)
			d++
		}
		// update
		cols[i] = s
	}
	data.Columns = cols
	data.Types = make([]string, len(cols))
	tp, err := rows.ColumnTypes()
	for i, t := range tp {
		dbn := t.DatabaseTypeName()
		data.Types[i] = dbn
	}
	for rows.Next() {

		sl, err := rows.SliceScan()
		if err == gosql.ErrNoRows {
			return data, nil
		}
		if err != nil {
			return nil, err
		}

		ms := make([]*Field, 0)
		for i, v := range sl {

			if v == nil {
				ms = append(ms, &Field{})
				continue
			}
			var f string
			switch t := v.(type) {
			case []byte:
				if data.Types[i] == "BLOB" {
					f = base64.StdEncoding.EncodeToString(t)
				} else {
					f = string(t)
				}
			case string:
				f = t
			case int32:
				f = strconv.Itoa(int(t))
			case int64:
				f = strconv.Itoa(int(t))
			case time.Time:
				f = strconv.FormatInt(t.UnixMilli(), 10)
			default:
				return nil, fmt.Errorf("invalid type: %v", reflect.TypeOf(v))
			}
			vf := wrapperspb.StringValue{Value: f}
			ms = append(ms, &Field{Value: &vf})
		}

		if err != nil {
			log.Printf("Error in retrieving data : %v", err)
			return nil, err
		}

		data.Rows = append(data.Rows, &DataRow{Value: ms})
	}
	return data, nil
}

var supportedFunctions = []string{"getBinary", "ammnt_queryTableFirSpatial", "getSpatialIndexTilesChangedOfDates", "getSpatialIndexChangesOfDates", "updateService", "ammnt_getAllBranches", "ammnt_addBranch", "ammnt_deleteSpatialIndex", "ammnt_addSpatialIndexEffectiveDate", "deleteDisMsg", "ammnt_IdentifyOadServiceEntity", "getDisMsg", "ammnt_GetFirRevision", "ammnt_AddFirAmmnt", "ammnt_queryTableFir", "GetServiceEntityRevision", "GetServiceRevision", "ammnt_GetNumberOfTodaysCommits", "ammnt_getLastDataCommits", "QueryTable", "UpdateProperty", "UpdatePermission", "UpdateOrganization", "UpdateLangTransEntity", "SetLanguagePreference", "InheritService", "DeleteInheritedService", "ammnt_GetUserActivity", "ammnt_AddActivityDataRecord"}

func isSupportedFunction(name string) bool {
	supported := false
	for _, f := range supportedFunctions {
		if f == name {
			supported = true
			break
		}
	}
	return supported
}

func (s *ClientService) StoredProcedure(ctx context.Context, req *ClientStoredProcedureRequest) (*DataTable, error) {
	if !isSupportedFunction(req.Name) {
		return nil, fmt.Errorf("function not whitelisted, aborting")
	}
	// check cache
	cacheKey := fmt.Sprintf("%s-%s", req.Name, strings.Join(req.Input, "-"))
	cached, found := s.cache.Get(cacheKey)
	if s.cacheEnabled && found {
		log.Println("cache hit:" + cacheKey)
		return cached.(*DataTable), nil
	}

	args := make([]any, 0)
	query := fmt.Sprintf("CALL %s(", req.Name)
	for x, s := range req.Input {
		query += "?"
		args = append(args, s)
		if x+1 == len(req.Input) {
			query += ")"
		} else {
			query += ","
		}
	}

	log.Println(query)
	log.Println(args)
	stmt, err := s.client.PreparexContext(ctx, query)
	if err != nil {
		return nil, err
	}
	rows, err := stmt.QueryxContext(ctx, args...)
	if err != nil {
		return nil, err
	}

	res, err := parseRows(rows)
	if err != nil {
		return nil, err
	}
	log.Println(res)
	// add to cache
	s.cache.Set(cacheKey, res, 0)
	return res, err
}

func (s *ClientService) InsecureQuery(ctx context.Context, in *InsecureQueryRequest) (*DataTable, error) {
	if in.Db != s.dbName {
		return nil, fmt.Errorf("invalid db name: %s", in.Db)
	}
	log.Println(in.Query)
	// check cache
	cacheKey := in.Query
	cached, found := s.cache.Get(cacheKey)
	if s.cacheEnabled && found {
		log.Println("cache hit:" + cacheKey)
		return cached.(*DataTable), nil
	}

	rows, err := s.client.QueryxContext(ctx, in.Query)
	if err != nil {
		return nil, err
	}

	res, err := parseRows(rows)
	if err != nil {
		return nil, err
	}
	log.Println(res)
	// add to cache
	s.cache.Set(cacheKey, res, 0)
	return res, err
}
