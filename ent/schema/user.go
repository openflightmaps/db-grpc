package schema

import (
	"entgo.io/contrib/entproto"
	"entgo.io/ent"
	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
)

// User holds the schema definition for the User entity.
type User struct {
	ent.Schema
}

func (User) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").Unique().
			StorageKey("UserID").
			SchemaType(map[string]string{
				dialect.MySQL: "int", // Override MySQL.
			}).
			Annotations(
				entproto.Field(1),
			),

		field.String("Username").
			StorageKey("Username").
			MaxLen(20).
			Annotations(
				entproto.Field(3),
			),

		field.String("Password").
			StorageKey("Password").
			MaxLen(20).
			Annotations(
				entproto.Field(4),
			),

		field.Int32("MainlanguagePref").
			StorageKey("MainlanguagePref").
			Optional().
			Annotations(
				entproto.Field(5),
			),
	}
}

func (User) Edges() []ent.Edge {
	return nil
}

func (User) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entproto.Message(),
		entproto.Service(
			entproto.Methods(entproto.MethodGet | entproto.MethodList),
		),
		entsql.Annotation{Table: "U1T"},
		entsql.Annotation{
			Charset:   "utf8",
			Collation: "utf8_general_ci",
		},
	}
}
