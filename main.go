package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"

	"entgo.io/ent/dialect/sql"

	"gitlab.com/openflightmaps/db-grpc/ent"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/entpb"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"
	"gitlab.com/openflightmaps/db-grpc/internal"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"google.golang.org/grpc"
)

func start() error {
	// Load config.yaml
	cfg, err := internal.LoadConfig()
	if err != nil {
		return err
	}

	// Initialize an sqlx client.
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s", cfg.DB.User, cfg.DB.Password, cfg.DB.Host, cfg.DB.Port, cfg.DB.DBName, cfg.DB.Options)
	drv, err := sql.Open(cfg.DB.Type, dsn)
	if err != nil {
		log.Fatalf("failed to open db connection: %v", err)
	}
	defer drv.Close()

	// Initialize an ent sql client.
	client, err := ent.NewClient(ent.Driver(drv)), nil
	if err != nil {
		log.Fatalf("failed create ent client: %v", err)
	}
	defer client.Close()

	// Run the migration tool (creating tables, etc).
	if err := client.Schema.WriteTo(context.Background(), os.Stdout); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	// Initialize the generated User service.
	svc := entpb.NewUserService(client)

	legacyClientSrv := legacy.NewClientService(sqlx.NewDb(drv.DB(), cfg.DB.Type), cfg.DB.DBName)

	// Create a new gRPC server (you can wire multiple services to a single server).
	server := grpc.NewServer()

	// Register the User service with the server.
	entpb.RegisterUserServiceServer(server, svc)

	// Register the Legacy client service
	legacy.RegisterClientServiceServer(server, legacyClientSrv)

	// Open port 5000 for listening to traffic.
	lis, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("failed listening: %s", err)
	}

	// Listen for traffic indefinitely.
	if err := server.Serve(lis); err != nil {
		log.Fatalf("server ended: %s", err)
	}

	return nil
}

func main() {
	if err := start(); err != nil {
		panic(err)
	}
}
