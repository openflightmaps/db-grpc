package internal

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	DB struct {
		Type     string
		Host     string
		Port     int
		User     string
		Password string
		DBName   string
		Options  string
	}
}

func LoadConfig() (*Config, error) {
	// load config.yaml from local dir or config subdir
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/config")
	viper.AddConfigPath("config")
	viper.AddConfigPath(".")

	viper.SetDefault("DB.Port", 3306)

	if err := viper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("failed to load config: %v", err)
	}

	cfg := Config{}
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, fmt.Errorf("invalid config: %v", err)
	}

	return &cfg, nil
}
