# db-grpc

## Description
# Open flightmaps DB micro service

## Purpose
Micro-Service for DB access. Accessed by the graphql api service.

## Notes
* Currently only supports the legacy functions needed by the OFM client. (stored procedures)
* ent ORM mapper will be used in the future
