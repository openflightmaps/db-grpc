package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/openflightmaps/db-grpc/ent"

	_ "github.com/mattn/go-sqlite3"
)

func CreateUser(ctx context.Context, client *ent.Client) (*ent.User, error) {
	u, err := client.User.
		Create().
		SetUsername("testuser").
		SetPassword("secret").
		Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating user: %w", err)
	}
	log.Println("user was created: ", u)
	return u, nil
}

func main() {
	client, err := ent.Open("sqlite3", "file:ent.db?cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()
	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	ctx := context.TODO()
	_, err = CreateUser(ctx, client)
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
}
