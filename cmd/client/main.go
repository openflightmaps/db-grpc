package main

import (
	"context"
	"log"
	"math/rand"
	"time"

	"gitlab.com/openflightmaps/db-grpc/ent/proto/entpb"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"

	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	// Open a connection to the server.
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed connecting to server: %s", err)
	}
	defer conn.Close()

	// Create a Legacy Client service on the connection.
	lc := legacy.NewClientServiceClient(conn)

	// create user service client
	uc := entpb.NewUserServiceClient(conn)

	ctx := context.Background()

	loaded, err := uc.Get(ctx, &entpb.GetUserRequest{Id: 0})
	if err != nil {
		se, _ := status.FromError(err)
		log.Fatalf("failed loading user: status=%s message=%s", se.Code(), se.Message())
	}
	log.Println(loaded)

	queried, err := lc.StoredProcedure(ctx, &legacy.ClientStoredProcedureRequest{Name: "QueryTable", Input: []string{loaded.Username, loaded.Password, "U2T", "-1", "-1", "-1"}})
	if err != nil {
		se, _ := status.FromError(err)
		log.Fatalf("failed creating user: status=%s message=%s", se.Code(), se.Message())
	}
	log.Println(queried)
}
